# coding: utf-8

from setuptools import setup, find_packages

NAME = "thermopi_rest"
VERSION = "1.0.0"

# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["connexion"]

setup(
    name=NAME,
    version=VERSION,
    description="Thermopi REST",
    author_email="vincent.bauchart@laposte.net",
    url="",
    keywords=["Swagger", "Thermopi REST"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['thermopi_rest/swagger/swagger.yaml']},
    include_package_data=True,
    long_description="""\
    Simple API to access GPIO from a Raspberry Pi. Embed DHT sensor lib
    """
)

