FROM alpine

RUN apk update && \
    apk add python3-dev py3-pip musl-dev git gcc && \
    rm -rf /var/cache/apk/

RUN pip3 install --upgrade pip setuptools

RUN git clone https://github.com/adafruit/Adafruit_Python_DHT.git && \
    cd Adafruit_Python_DHT && \
    python3 setup.py install

ADD . /thermopi

WORKDIR /thermopi

RUN pip3 install wheel && \
    pip3 install -r requirements.txt

RUN python3 setup.py install
CMD python3 -m thermopi_rest

