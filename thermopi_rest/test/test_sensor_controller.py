# coding: utf-8

from __future__ import absolute_import

from thermopi_rest.models.sensor import Sensor
from . import BaseTestCase
from six import BytesIO
from flask import json


class TestSensorController(BaseTestCase):
    """ SensorController integration test stubs """

    def test_get_sensor(self):
        """
        Test case for get_sensor

        Get Sensor information
        """
        response = self.client.open(
            "/rest/dht/{gpioID}".format(gpioID=789), method="GET"
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    import unittest

    unittest.main()
