# coding: utf-8

from __future__ import absolute_import

from thermopi_rest.models.gpio import Gpio
from thermopi_rest.models.state import State
from . import BaseTestCase
from six import BytesIO
from flask import json


class TestGpioController(BaseTestCase):
    """ GpioController integration test stubs """

    def test_get_gpio(self):
        """
        Test case for get_gpio

        get GPIO state
        """
        response = self.client.open(
            "/rest/gpio/{gpioID}".format(gpioID=789), method="GET"
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_set_gpio(self):
        """
        Test case for set_gpio

        set GPIO state
        """
        state = State()
        response = self.client.open(
            "/rest/gpio/{gpioID}".format(gpioID="gpioID_example"),
            method="PUT",
            data=json.dumps(state),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    import unittest

    unittest.main()
