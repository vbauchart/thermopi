import connexion

from thermopi_rest.models.gpio import Gpio
from thermopi_rest.models.state import State
import RPi.GPIO as GPIO


def get_gpio(gpioID):
    """
    get GPIO state
    get GPIO state
    :param gpioID: ID of GPIO port
    :type gpioID: int

    :rtype: Gpio
    """
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gpioID, GPIO.IN)
    state = GPIO.input(gpioID)
    GPIO.cleanup()
    return Gpio(gpioID, True)


def set_gpio(gpioID, state):
    """
    set GPIO state
    Manipulate GPIO State
    :param gpioID: GPIO that need to be updated
    :type gpioID: str
    :param state: Updated user object
    :type state: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        state = State.from_dict(connexion.request.get_json())
    return "do some magic!"
