from thermopi_rest.dht.sensor_factory import SensorFactory


def get_sensor(gpioID, sensor_type=None):
    """
    Get Sensor information
    Returns Sensor information
    :param gpioID: ID of GPIO port
    :type gpioID: int
    :param sensor_type: Type of Sensor
    :type sensor_type: str

    :rtype: Sensor
    """
    s = SensorFactory.get_sensor(gpioID, sensor_type)
    return s.read_sensor()
