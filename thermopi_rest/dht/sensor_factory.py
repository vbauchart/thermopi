from thermopi_rest.dht.sensor_fake import SensorFake


class SensorFactory:
    @staticmethod
    def get_sensor(gpio_id, sensor_type):

        if sensor_type == "RANDOM" or sensor_type is None:
            return SensorFake(gpio_id)
        elif sensor_type == "DHT22":
            try:
                from thermopi_rest.dht.sensor_dht import SensorDHT

                return SensorDHT(gpio_id, 22)
            except:
                print("DHT22 is not avaiable on this platform")
        else:
            raise SensorUnknownException()


class SensorUnknownException(Exception):
    pass
