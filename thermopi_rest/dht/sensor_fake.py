from datetime import datetime
from time import sleep

from thermopi_rest.models import Sensor


class SensorFake:
    def __init__(self, gpio_id):
        self.gpio_id = gpio_id

    def read_sensor(self):
        sleep(2)
        humidity = 45
        temperature = 22
        return Sensor(temperature, humidity, self.gpio_id, datetime.now())
