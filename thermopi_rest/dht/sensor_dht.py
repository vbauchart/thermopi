from datetime import datetime

from Adafruit_DHT import read_retry

from thermopi_rest.models import Sensor


class SensorDHT:
    def __init__(self, gpio_id, sensor_type):
        self.gpio_id = gpio_id
        self.sensor_type = sensor_type

    def read_sensor(self):
        humidity, temperature = read_retry(self.sensor_type, self.gpio_id)
        return Sensor(temperature, humidity, self.gpio_id, datetime.now())
